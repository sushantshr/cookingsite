<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/generate/models', '\\Jimbolino\\Laravel\\ModelBuilder\\ModelGenerator5@start');

// routes before any of the users login
route::group(['middleware'=>['beforelogin']], function()
{
    //route to add the users
    route::get('/user/add','usercontroller@insertdata')->name('ad');
    route::post('/user/add/process', ['uses'=>'usercontroller@adduser', 'as'=>'addu']);

    //routes for login page
    route::get('/login', ['uses'=>'logincontroller@index', 'as'=>'loginpage']);
    route::post('/login/process', ['uses'=>'logincontroller@login1', 'as'=>'logg']);

    //route for social site login
    Route::get('auth/{provider}', 'logincontroller@redirectToProvider');
    Route::get('auth/{provider}/callback', 'logincontroller@handleProviderCallback');

});


//route after any user is log in
route::group(['middleware'=>['afterlogin']], function() {
    //route for admin
    route::group(['middleware'=>'isadmin'], function (){

        route::get('/admin/list/users', ['uses'=>'adminController@listUser', 'as'=>'list.user']);
        route::get('/admin/list/videos', ['uses'=>'adminController@listVideo', 'as'=>'list.video']);
        route::view('/admin','frontend.pages.admin.admin')->name('view.admin');
        route::get('/change/role/{id}',['uses'=>'adminController@changerole', 'as'=>'changerole']);






    });

    //route for users
    route::group(['middleware'=>'isuser'], function () {

        route::view('/user','frontend.pages.user.user');

        route::get('/user/history/{id}',['uses'=>'userController@getHistory', 'as'=>'get.history'] );





    });

    route::get('/upload/video/{id}', ['uses'=>'userController@uploadVideoInfo', 'as'=>'get.upload']);
    route::post('upload/video', ['uses'=>'userController@uploadVideo', 'as'=>'post.upload']);
    route::get('/user/edit/{id}', 'usercontroller@edituserdetail')->name('edit');
    route::post('/user/edit/{id}','usercontroller@edituser')->name('editu');

    route::get('/user/getinfo/{id}', 'usercontroller@getoneuserdetail')->name('listinfo');

    route::get('/user/del/{id}', ['uses'=>'usercontroller@deleteuser', 'as'=>'deactivate']);
    route::get('/logout', ['uses'=>'logincontroller@logout', 'as'=>'logout']);

    route::get('/view/byanjan', ['uses'=>'usercontroller@viewByanjan', 'as'=>'view.byanjan']);
    route::get('/view/one/byanjan/{id}', ['uses'=>'usercontroller@viewSingle', 'as'=>'view.single']);
    route::get('/delete/video/{id}', ['uses'=>'usercontroller@deleteVideo', 'as'=>'delete.video']);
    route::get('/byanjan/search',['uses'=>'youtubeController@searchByanjan', 'as'=>'search.byanjan']);
    route::post('/calculate', ['uses'=>'usercontroller@calculateIng', 'as'=>'calculate']);





});


//route for frontpages
route::view('/index', 'frontend.pages.index')->name('indexpage');
route::view('/about', 'frontend.pages.about')->name('aboutpage');
route::view('/contact', 'frontend.pages.contact')->name('contactpage');

//route to manage youtube videos
route::get('/channel/list/{page?}', ['uses'=>'youtubeController@listChannel'])->name('videopage');
route::get('/watch/youtubevideo/{id}/{name}',function ($id, $name)
{
    return view('frontend.pages.watchyoutube', compact('id', 'name'));
})->name('watch.youtube');

route::get('/youtube/search', ['uses'=>'youtubeController@searchYoutube', 'as'=>'search.youtube']);
route::get('/youtube/more/{id}/{page?}', ['uses'=>'youtubeController@getChannel', 'as'=>'watchmore']);

//send mail
Route::post('/email/send', ['uses'=>'emailController@sendMail', 'as'=>'send.mail']);

