-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 07, 2017 at 09:57 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cooksite`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Breakfast'),
(2, 'Brunch'),
(3, 'Lunch'),
(4, 'Snacks'),
(5, 'Appetizers'),
(6, 'Dinner'),
(7, 'Soups'),
(8, 'Noodles'),
(9, 'Rice'),
(10, 'Pasta'),
(11, 'Meat'),
(12, 'Poutry'),
(13, 'Seafood'),
(14, 'Vegetrian'),
(15, 'Salads'),
(16, 'Sides'),
(17, 'Sauces'),
(18, 'Baking'),
(19, 'Desert'),
(20, 'Drinks');

-- --------------------------------------------------------

--
-- Table structure for table `channels`
--

CREATE TABLE `channels` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `channel_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `channels`
--

INSERT INTO `channels` (`id`, `name`, `channel_id`) VALUES
(1, 'Food Network UK', 'UCAwQMF0bRy04OxKroB8EX2Q'),
(2, 'Gordon Ramsay', 'UCIEv3lZ_tNXHzL3ox-_uUGQ'),
(3, 'Epic Meal Time', 'UCYjk_zY-iYR8YNfJmuzd70A'),
(4, 'zkhanakhazana', 'UCtMVgKecqimjQIaC9E9kFFQ'),
(5, 'How To Cook That', 'UCsP7Bpw36J666Fct5M8u-ZA'),
(6, 'POPSUGAR Food', 'UC1aJuxLHlw8bBV6mfCqVfog'),
(7, 'Tasty and easy food recipes', 'UCN_mEa9cavDEBpiSs0q361A'),
(8, 'SimpleCookingChannel', 'UCsWpnu6EwIYDvlHoOESpwYg'),
(10, 'MyCupcakeAddiction', 'UCxytOEPP99jj8mqVGAO7haQ'),
(11, 'Turkish Food Recipes', 'UCwVIj-OqEjXNqKpItChRrMQ'),
(12, 'The French Cooking Academy', 'UC0lG3Ihe4LGV851lODRIS5g'),
(13, 'DIGITAL FOOD NETWORK', 'UCG9iqDBhEks_MOzsbjE73xg'),
(14, 'Cooking A Dream', 'UCAfw7OYXP45Qktu6hFHEPlA'),
(15, 'Iam a Big Foodie', 'UCD__OiTROzWHevdq5qCwULQ'),
(16, 'Food Network Asia', 'UCRG4sudMtCnunYKT7xVmpgg'),
(17, 'Tasty', 'UCJFp8uSYCjXOMnkUyb3CQ3Q'),
(18, 'How To Cake It', 'UCvM1hVcRJmVWDtATYarC0KA'),
(19, 'Famous Cuisines Channel', 'UCZpYkzu03zxADU-4FaN7waA');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `video_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `comment`, `video_id`, `parent_id`, `user_id`, `status_id`) VALUES
(1, 'Nice Omlete', 7, 0, 18, 3);

-- --------------------------------------------------------

--
-- Table structure for table `ingredient`
--

CREATE TABLE `ingredient` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `qty` float NOT NULL,
  `unit` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ingredient`
--

INSERT INTO `ingredient` (`id`, `name`, `qty`, `unit`) VALUES
(4, 'tealeaves', 0.5, 'teaspoon'),
(5, 'bread', 12, 'kilogram'),
(6, 'bread', 12, 'gram'),
(7, 'milk', 1, 'litre'),
(23, 'soaked basmati rice', 1, 'cup'),
(24, 'green peas', 10, 'pieces'),
(25, 'bay leaf', 2, 'pieces'),
(26, 'red chilly', 1, 'pieces'),
(27, 'cardamom', 3, 'pieces'),
(28, 'cloves', 4, 'pieces'),
(29, 'raisin', 10, 'pieces'),
(30, 'cashew', 10, 'pieces'),
(31, 'cardamon', 3, 'pieces'),
(32, 'salt', 1, 'teaspoon'),
(33, 'sugar', 10, 'teaspoon'),
(34, 'butter', 0.25, 'cup'),
(43, 'olive oil', 1, 'tablespoon'),
(44, 'minced ginger', 1, 'teaspoon'),
(45, 'Red Onion', 1, 'unit'),
(46, 'tomato', 1.5, 'unit'),
(47, 'Green chillies', 2, 'unit'),
(48, 'corainder(as per wish)', 1, 'unit'),
(49, 'water', 1.5, 'cup'),
(50, 'wai wai', 1, 'unit'),
(54, 'egg', 2, 'unit'),
(55, 'sliced cheese', 1, 'gram'),
(56, 'salt', 1, 'teaspoon'),
(57, 'pasta', 1.5, 'cup'),
(58, 'boiled sweet corn', 3, 'tablespoon'),
(59, 'chopped carrot', 2, 'tablespoon'),
(60, 'chopped capsicum', 0.25, 'cup'),
(61, 'chopped onion', 0.25, 'cup'),
(62, 'tomato puree', 1, 'cup'),
(63, 'tomato ketchup', 1, 'tablespoon'),
(64, 'oregano', 0.25, 'tablespoon'),
(65, 'grated ginger', 0.5, 'teaspoon'),
(66, 'grated garlic', 1.5, 'teaspoon'),
(67, 'red chilli powder', 1, 'teaspoon'),
(68, 'garam masala', 0.5, 'teaspoon'),
(69, 'turmeric powder', 0.25, 'teaspoon'),
(70, 'oil', 3, 'tablespoon'),
(71, 'salt', 1, 'teaspoon'),
(72, 'oil', 4, 'tablespoon'),
(73, 'ginger paste', 1, 'teaspoon'),
(74, 'green chilly paste', 0.5, 'teaspoon'),
(75, 'chopped cabbage', 1, 'cup'),
(76, 'sweet corn kernels', 1, 'cup'),
(77, 'chopped green capsium', 0.5, 'cup'),
(78, 'chopped red capsium', 0.5, 'cup'),
(79, 'chopped yellow capsium', 0.5, 'cup'),
(80, 'salt', 0.5, 'teaspoon'),
(81, 'Black paper powder', 0.5, 'teaspoon'),
(82, 'white paper powder', 0.5, 'teaspoon'),
(83, 'red chilly flakes', 1, 'tablespoon'),
(84, 'vinegar', 1, 'tablespoon'),
(85, 'tomato sauce', 0.5, 'cup'),
(86, 'grated cheese', 1, 'cup'),
(87, 'boiled rice', 2, 'cup'),
(88, 'chopped green corindar', 0.25, 'cup'),
(89, 'bread', 1, 'pound'),
(90, 'egg', 3, 'unit'),
(91, 'paneer', 10, 'pieces'),
(92, 'oil', 4, 'tablespoon'),
(93, 'capsium', 0.5, 'cup'),
(94, 'onion', 0.5, 'cup'),
(95, 'green chilli', 2, 'pieces'),
(96, 'tomato chilli sauce', 2, 'tablespoon'),
(97, 'garlic paste', 0.5, 'teaspoon'),
(98, 'ginger paste', 0.5, 'teaspoon'),
(99, 'soya sauce', 1, 'tablespoon'),
(100, 'salt', 0.5, 'teaspoon'),
(101, 'corn flour', 0.5, 'cup'),
(102, 'flour', 1, 'cup'),
(103, 'oil', 4, 'tablespoon'),
(104, 'ginger paste', 1, 'teaspoon'),
(105, 'green chilli paste', 0.5, 'teaspoon'),
(106, 'chopped cabbage', 1, 'cup'),
(107, 'sweet corn kernels', 1, 'cup'),
(108, 'chopped green capsium', 0.5, 'cup'),
(109, 'chopped red capsium', 0.5, 'cup'),
(110, 'chopped yellow capsium', 0.5, 'cup'),
(111, 'salt', 0.5, 'teaspoon'),
(112, 'Black  pepper powder', 0.5, 'teaspoon'),
(113, 'white pepper powder', 0.5, 'teaspoon'),
(114, 'red chilli flakes', 1, 'tablespoon'),
(115, 'vinegar', 1, 'tablespoon'),
(116, 'tomato sauce', 0.5, 'cup'),
(117, 'grated cheese', 1, 'cup'),
(118, 'boiled rice', 1, 'cup'),
(119, 'chopped green corindar', 0.25, 'cup'),
(120, 'boiled pasta', 1, 'cup'),
(121, 'boiled sweet corn', 3, 'tablespoon'),
(122, 'chopped carrot', 2, 'tablespoon'),
(123, 'chopped capsicum', 0.25, 'cup'),
(124, 'chopped onion', 0.25, 'cup'),
(125, 'tomato puree', 1, 'cup'),
(126, 'tomato ketchup', 1, 'tablespoon'),
(127, 'oregano', 0.25, 'tablespoon'),
(128, 'grated ginger', 0.5, 'teaspoon'),
(129, 'grated garlic', 0.5, 'teaspoon'),
(130, 'red chilli powder', 1, 'teaspoon'),
(131, 'garam masala', 0.5, 'teaspoon'),
(132, 'turmeric powder', 0.25, 'teaspoon'),
(133, 'oil', 3, 'tablespoon'),
(134, 'salt', 1, 'teaspoon'),
(135, 'olive oil', 1, 'tablespoon'),
(136, 'mincedginger', 1, 'teaspoon'),
(137, 'red onion', 1.5, 'unit'),
(138, 'green chiliies', 2, 'unit'),
(139, 'coriander', 1, 'unit'),
(140, 'water', 1.5, 'cup'),
(141, 'wai wai', 1, 'unit'),
(142, 'chicken legs', 2, 'pieces'),
(143, 'salt to taste', 1, 'pinch'),
(144, 'red chilly powder', 0.5, 'teaspoon'),
(145, 'lemon juice', 2, 'teaspoon'),
(146, 'turmeric powder', 0.25, 'pinch'),
(147, 'thick yoghurt', 4, 'cup'),
(148, 'red chilly powder', 0.5, 'teaspoon'),
(149, 'ginger garlic crushed/paste', 1, 'tablespoon'),
(150, 'coriander cumin powder', 0.5, 'teaspoon'),
(151, 'garam masala', 0.5, 'teaspoon'),
(152, 'lemon juice', 1, 'teaspoon'),
(153, 'chat masala', 0.5, 'teaspoon'),
(154, 'dry mango powder', 0.5, 'teaspoon'),
(155, 'tandoori masala', 1, 'teaspoon'),
(156, 'fenugreek leaves', 2, 'teaspoon'),
(157, 'black pepper powder', 0.25, 'teaspoon'),
(158, 'oil', 2, 'tablespoon'),
(159, 'few pinches of red food color', 0, 'pinch'),
(160, 'all purpose flour', 1.5, 'cup'),
(161, 'all purpose flour', 1.5, 'cup'),
(162, 'sugar(powdered)', 1.5, 'cup'),
(163, 'sugar(powdered)', 1.5, 'cup'),
(164, 'cocoa powder', 3, 'tablespoon'),
(165, 'cocoa powder', 3, 'tablespoon'),
(166, 'baking soda', 1, 'teaspoon'),
(167, 'baking soda', 1, 'teaspoon'),
(168, 'salt', 0.25, 'teaspoon'),
(169, 'salt', 0.25, 'teaspoon'),
(170, 'oil', 0.5, 'cup'),
(171, 'warm water', 1, 'cup'),
(172, 'oil', 0.5, 'cup'),
(173, 'instant coffee powder', 1, 'teaspoon'),
(174, 'warm water', 1, 'cup'),
(175, 'vinegar', 2, 'teaspoon'),
(176, 'instant coffee powder', 1, 'teaspoon'),
(177, 'vanilla extract', 1, 'teaspoon'),
(178, 'vinegar', 2, 'teaspoon'),
(179, 'vanilla extract', 1, 'teaspoon'),
(180, 'Any tea biscuits', 250, 'gram'),
(181, 'Chocolate syrup/sauce', 0.25, 'cup'),
(182, 'instant coffee powder', 1, 'teaspoon'),
(183, 'dessicated coconut', 0.5, 'cup'),
(184, 'powdered sugar', 0.25, 'cup'),
(185, 'butter', 2, 'tablespoon'),
(186, 'cardamom powder', 0.25, 'teaspoon'),
(187, 'some milk/water as desired', 0.5, 'cup'),
(188, 'onion', 1, 'cup'),
(189, 'tomato', 0.5, 'cup'),
(190, 'onion', 0.5, 'cup'),
(191, 'paneer', 10, 'pieces'),
(192, 'salt', 1, 'teaspoon'),
(193, 'oil', 4, 'tablespoon'),
(194, 'pea', 0.25, 'cup'),
(195, 'garlic paste', 1, 'teaspoon'),
(196, 'ginger', 0.5, 'teaspoon'),
(197, 'corriander', 0.25, 'cup'),
(198, 'red chilli powder', 1, 'teaspoon'),
(199, 'turmeric powder', 0.25, 'teaspoon'),
(200, 'curd', 0.5, 'cup'),
(201, 'cumin powder', 1, 'teaspoon'),
(202, 'holy spice powder', 1, 'tablespoon');

-- --------------------------------------------------------

--
-- Table structure for table `recipie`
--

CREATE TABLE `recipie` (
  `id` int(11) NOT NULL,
  `url` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `ingredient_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `role` int(11) NOT NULL DEFAULT '1',
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `role`, `user_id`) VALUES
(4, 0, 4),
(7, 1, 15),
(8, 1, 16),
(10, 2, 18),
(11, 1, 19);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `condition` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `condition`) VALUES
(1, 'Inactive'),
(2, 'Pending'),
(3, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `userlog`
--

CREATE TABLE `userlog` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `log` varchar(255) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userlog`
--

INSERT INTO `userlog` (`id`, `user_id`, `log`, `updated_at`, `created_at`) VALUES
(8, 15, 'a:6:{i:0;s:1:\"1\";i:1;s:1:\"5\";i:2;s:2:\"11\";i:3;s:2:\"10\";i:4;s:1:\"3\";i:5;s:2:\"12\";}', '2017-11-06 23:38:19', '2017-10-30 01:10:16'),
(9, 16, 'a:7:{i:0;s:1:\"2\";i:1;s:1:\"9\";i:2;s:1:\"5\";i:3;s:1:\"7\";i:4;s:1:\"8\";i:5;s:2:\"11\";i:6;s:2:\"14\";}', '2017-11-06 04:46:59', '2017-10-30 02:34:45'),
(10, 18, 'a:5:{i:0;s:1:\"3\";i:1;s:1:\"5\";i:2;s:1:\"7\";i:3;s:2:\"15\";i:4;s:2:\"19\";}', '2017-11-07 01:36:24', '2017-11-01 01:42:12'),
(11, 19, 'a:2:{i:0;s:2:\"17\";i:1;s:2:\"18\";}', '2017-11-07 00:47:16', '2017-11-07 00:47:07');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `provider` varchar(255) NOT NULL,
  `provider_id` varchar(255) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `status_id` int(11) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `email`, `provider`, `provider_id`, `avatar`, `status_id`, `remember_token`) VALUES
(4, 'sushant', '$2y$10$iBZ2wCPMI8ZCJDRr0/NQR.QOYp6YguyKQ3Io2wHXAOxybelwqzDfu', 'sushant@gmail.com', 'cookingsite', '001', '/avatarimg/m.jpg', 3, '5EjDm0lD6x1QVwi1VdgqSgFmFMbbl0vYxHUqYr6u2PCY4Ge9re2m1Xt6CeDj'),
(15, 'sakroj', '$2y$10$egvxsnqg3eP1P14tvNRl5eCN4Zd5EG6JfVVbNo6yafP/uF/eAico2', 'sakroj@gmail.com', 'cookingsite', '001', '/avatarimg/0.07854400 1509848152.JPG', 3, 'NFA90x3YVCLiJGrRd5gcyWj0GJmZJDAWJdK636HR9LlybaNx6EECpwUyKnIh'),
(16, 'pranita tamang', '$2y$10$MOuGCxChcUf.kS9QN.00vOAf7iN7M/wnNQ.g7ZdZuFGAiUzXhPYXS', 'tamangpranita18@gmail.com', 'google', '114264154896589504990', '/avatarimg/0.33897300 1510032998.jpg', 3, 'lSkRAkvM9ut7iDL186lLTRCGvSdP9VUWxHeBplzLUk7v3cJDhW6caSIZLJ2d'),
(18, 'Sushant Shrestha', '$2y$10$DH/Hi.G36CLhnohkK/9SLeWkYlaPUb4i5B0TIFJWF0kgoug8/niS.', 'sushantshr01@gmail.com', 'facebook', '1600042870062252', 'https://graph.facebook.com/v2.10/1600042870062252/picture?width=1920', 3, '6lKSN01c57FXrsqAKBAeuqGMLH2TXCiTaQRQkEmS2HC6SwBijw90Z93VYQmY'),
(19, 'sujan khatri', '$2y$10$xWWHmloVIauqcZ8Jttg3GOBWr0hfYFOlRZnr7QSVIokUQBXLkxRiq', 'sujankhatri14@gmail.com', 'google', '101943786763350729624', 'https://lh5.googleusercontent.com/-N_hTuTd-lTk/AAAAAAAAAAI/AAAAAAAAACY/JoNpX_DIACY/photo.jpg', 3, 'VfFGIH7br13UqMBzzvylOlxt6Fjz113DbjABSr5hLAiiq0RQlXP5hz7zKz1j');

-- --------------------------------------------------------

--
-- Table structure for table `user_detail`
--

CREATE TABLE `user_detail` (
  `id` int(11) NOT NULL,
  `about` text NOT NULL,
  `interest` text NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_detail`
--

INSERT INTO `user_detail` (`id`, `about`, `interest`, `user_id`) VALUES
(4, 'brilliant cook', 'a:1:{i:0;s:9:\"Breakfast\";}', 4),
(7, 'Im Sakroj Don 100%', 'a:1:{i:0;s:9:\"Breakfast\";}', 15),
(8, '<p><strong>i know <strong>kjklj</strong>everything</strong></p>', 'a:2:{i:0;s:9:\"Breakfast\";i:1;s:9:\"Vegetrian\";}', 16),
(10, '<p>Me!!!</p>', 'a:4:{i:0;s:9:\"Breakfast\";i:1;s:10:\"Appetizers\";i:2;s:4:\"Rice\";i:3;s:7:\"Seafood\";}', 18),
(11, '<p>It\'s just a brunch of moments we can share.</p>', 'a:9:{i:0;s:9:\"Breakfast\";i:1;s:5:\"Lunch\";i:2;s:6:\"Snacks\";i:3;s:10:\"Appetizers\";i:4;s:5:\"Pasta\";i:5;s:4:\"Meat\";i:6;s:6:\"Baking\";i:7;s:6:\"Desert\";i:8;s:6:\"Drinks\";}', 19);

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `view_count` int(11) NOT NULL DEFAULT '0',
  `status_id` int(11) NOT NULL DEFAULT '3',
  `img` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`id`, `name`, `url`, `category_id`, `user_id`, `view_count`, `status_id`, `img`) VALUES
(3, 'pulao', 'uploads/0.52128600 1509352954.WEBM', 1, 16, 4, 3, '/uploads/pulao.png'),
(7, 'Omlette', 'uploads/0.87007500 1509643907.MKV', 1, 18, 3, 3, '/uploads/0.86685400 1509643907.jpg'),
(10, 'bread omlette', 'uploads/0.36160900 1509847691.mp4', 1, 15, 0, 3, '/uploads/0.35698400 1509847691.png'),
(11, 'kimchi', 'uploads/0.24100700 1509847929.MKV', 1, 15, 1, 3, '/uploads/0.23795600 1509847929.png'),
(12, 'Chilli Paneer', 'uploads/0.05763500 1509848579.WEBM', 6, 16, 0, 3, '/uploads/0.05457200 1509848579.png'),
(13, 'cheese vegetable rice', 'uploads/0.51991000 1509849149.MKV', 6, 16, 0, 3, '/uploads/0.47962600 1509849149.png'),
(14, 'vegetable pasta', 'uploads/0.41894600 1509851414.MKV', 3, 16, 0, 3, '/uploads/0.41599500 1509851414.png'),
(15, 'wai wai', 'uploads/0.81648300 1509887430.MKV', 1, 18, 0, 3, '/uploads/0.81228600 1509887430.png'),
(16, 'Chicken Tandoori', 'uploads/0.38429800 1510035660.MKV', 11, 19, 0, 3, '/uploads/0.38050600 1510035660.png'),
(17, 'eggless chocolate cupcake', 'uploads/0.12817000 1510036312.MKV', 1, 19, 0, 3, '/uploads/0.12539700 1510036312.png'),
(19, 'Swiss roll cookie', 'uploads/0.33479900 1510036864.MKV', 1, 19, 1, 3, '/uploads/0.33158800 1510036864.png'),
(21, 'Matar Paneer', 'uploads/0.15011500 1510040667.mp4', 14, 18, 0, 3, '/uploads/0.14474000 1510040667.png');

-- --------------------------------------------------------

--
-- Table structure for table `video_ingredient`
--

CREATE TABLE `video_ingredient` (
  `video_id` int(11) NOT NULL,
  `ingredient_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `video_ingredient`
--

INSERT INTO `video_ingredient` (`video_id`, `ingredient_id`) VALUES
(3, 23),
(3, 24),
(3, 25),
(3, 26),
(3, 27),
(3, 28),
(3, 29),
(3, 30),
(3, 31),
(3, 32),
(3, 33),
(3, 34),
(7, 54),
(7, 55),
(7, 56),
(10, 89),
(11, 90),
(12, 91),
(12, 92),
(12, 93),
(12, 94),
(12, 95),
(12, 96),
(12, 97),
(12, 98),
(12, 99),
(12, 100),
(12, 101),
(12, 102),
(13, 103),
(13, 104),
(13, 105),
(13, 106),
(13, 107),
(13, 108),
(13, 109),
(13, 110),
(13, 111),
(13, 112),
(13, 113),
(13, 114),
(13, 115),
(13, 116),
(13, 117),
(13, 118),
(13, 119),
(14, 120),
(14, 121),
(14, 122),
(14, 123),
(14, 124),
(14, 125),
(14, 126),
(14, 127),
(14, 128),
(14, 129),
(14, 130),
(14, 131),
(14, 132),
(14, 133),
(14, 134),
(15, 135),
(15, 136),
(15, 137),
(15, 138),
(15, 139),
(15, 140),
(15, 141),
(16, 142),
(16, 143),
(16, 144),
(16, 145),
(16, 146),
(16, 147),
(16, 148),
(16, 149),
(16, 150),
(16, 151),
(16, 152),
(16, 153),
(16, 154),
(16, 155),
(16, 156),
(16, 157),
(16, 158),
(16, 159),
(17, 160),
(17, 162),
(17, 164),
(17, 166),
(17, 168),
(17, 170),
(17, 171),
(17, 173),
(17, 175),
(17, 177),
(19, 180),
(19, 181),
(19, 182),
(19, 183),
(19, 184),
(19, 185),
(19, 186),
(19, 187),
(21, 189),
(21, 190),
(21, 191),
(21, 192),
(21, 193),
(21, 194),
(21, 195),
(21, 196),
(21, 197),
(21, 198),
(21, 199),
(21, 200),
(21, 201),
(21, 202);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `channels`
--
ALTER TABLE `channels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `video_id` (`video_id`),
  ADD KEY `parent_id` (`parent_id`,`user_id`,`status_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indexes for table `ingredient`
--
ALTER TABLE `ingredient`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recipie`
--
ALTER TABLE `recipie`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`,`status_id`),
  ADD KEY `status_id` (`status_id`),
  ADD KEY `ingredient_id` (`ingredient_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userlog`
--
ALTER TABLE `userlog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `status_id` (`status_id`);

--
-- Indexes for table `user_detail`
--
ALTER TABLE `user_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `view_count` (`view_count`,`status_id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indexes for table `video_ingredient`
--
ALTER TABLE `video_ingredient`
  ADD KEY `video_id` (`video_id`),
  ADD KEY `ingredient_id` (`ingredient_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `channels`
--
ALTER TABLE `channels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ingredient`
--
ALTER TABLE `ingredient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=203;
--
-- AUTO_INCREMENT for table `recipie`
--
ALTER TABLE `recipie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `userlog`
--
ALTER TABLE `userlog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `user_detail`
--
ALTER TABLE `user_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comment_ibfk_3` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `recipie`
--
ALTER TABLE `recipie`
  ADD CONSTRAINT `recipie_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `recipie_ibfk_2` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `recipie_ibfk_3` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredient` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role`
--
ALTER TABLE `role`
  ADD CONSTRAINT `role_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `userlog`
--
ALTER TABLE `userlog`
  ADD CONSTRAINT `userlog_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_detail`
--
ALTER TABLE `user_detail`
  ADD CONSTRAINT `user_detail_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `video`
--
ALTER TABLE `video`
  ADD CONSTRAINT `video_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `video_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `video_ibfk_3` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `video_ingredient`
--
ALTER TABLE `video_ingredient`
  ADD CONSTRAINT `video_ingredient_ibfk_1` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredient` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `video_ingredient_ibfk_2` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
