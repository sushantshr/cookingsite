@component('mail::message')
#{{ $name }}
###{{ $mail }} has messaged for
{{ $body }}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
