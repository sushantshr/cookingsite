<!DOCTYPE html>
<html lang="en">
<head>
    <title>Byanjan</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/custom.css">
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.0.0-beta.3/dist/css/bootstrap-material-design.min.css" integrity="sha384-k5bjxeyx3S5yJJNRD1eKUMdgxuvfisWKku5dwHQq9Q/Lz6H8CyL89KF52ICpX4cL" crossorigin="anonymous">
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">--}}
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>
<body style="border-spacing: 0px;padding: 0px;">

@include('frontend.include.header')
<div class="row" style="min-height:550px;margin-right: 0px;padding-top:70px; ">
    <div class="col-lg-2" style="background: #f5f5f5;">
        @include('frontend.include.sidebar')
    </div>
    <div class="col-lg-10" id="mainbody" style="background: url({{URL::asset("img/2.jpg")}});background-size: 1150px 550px;">

        @yield('content')
    </div>
      @include('frontend.include.calculatebar')
</div>

@include('frontend.include.footer')
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
@yield('script')
<script>
    $(function(){

        $("#rightbutton").click(
            function()
            {

                $("#rightbutton").animate(
                    {left:'97%'}, "slow", function()
                    {
                        $("#rightbutton").hide().css("left", "57%");
                    }
                );

                $("#panel").animate(
                    {left: '95%', opacity:"0", width:"toggle"}, "slow", function()
                    {
                        $("#leftbutton").show();
                        $("#mainpanel").css(
                            {
                                'width':'0%',
                                'height':'0%',
                                'left':'0%'
                            }
                        );
                    }
                );

            });
        $("#leftbutton").click( function(){
            $("#mainpanel").css(
                {
                    'width':'40%',
                    'height':'100%',
                    'left':'58%'
                }
            );
            $("#leftbutton").animate(
                {left:'57%'}, "slow", function()
                {
                    $("#leftbutton").hide().css("left", "97%");
                }
            );

            $("#panel").animate(
                {left: '5%', opacity:"1", width:"toggle"}, "slow", function()
                {
                    $("#rightbutton").show();
                }
            );

        })
    });
</script>
<script>
    function remove()
    {
        $last = document.getElementById('table');
        console.log($last.children.length);
        if($last.children.length>1)
        {
            $child = $last.lastChild;
            $last.removeChild($child);
            if($last.children.length==1)
            {
                document.getElementById('calculateIng').disabled=true;
            }
        }
    }
    function add() {
        var submit = document.getElementById('calculateIng');
        console.log(submit);
        if(submit.disabled===true)
            submit.disabled=false;
        var row = document.createElement("tr");
        var input1 = document.createElement("input");
        input1.required = true;
        input1.type = "text";
        input1.style.width = "102%";
        input1.placeholder = "Ingredient name ....";

        var input2 = document.createElement("input");
        input2.required = true;
        input2.type = "text";
        input2.placeholder = "quantity here ....";

        var ing = document.createElement("td");
        input1.name = "ing[]";
        ing.appendChild(input1);

        var qty = document.createElement("td");
        input2.name = "qty[]";
        input2.onchange= function ()
        {
            var val = this.value;
            if(isNaN(val))
            {
                this.value = 0;
                alert("must input number");
            }
        };
        qty.appendChild(input2);

        var selecttd = document.createElement("td");
        var select = document.createElement("select");
        select.name="unit[]";
        var option = [];

        var i=0;
        var unit = ["kilogram", "gram", "pound", "litre", "millilitre","teaspoon","tablespoon", "metre", "centimetre", "millimeter","cup", "pieces", "pinch", "unit"];
        for (i = 0; i < unit.length; i=i+1)
        {
            option[i] = document.createElement("option");
            option[i].value=unit[i];
            option[i].innerHTML = unit[i];
            select.appendChild(option[i]);
        }
        selecttd.appendChild(select);
        var table = document.getElementById("table");

        row.appendChild(ing);
        row.appendChild(qty);
        row.appendChild(selecttd);

        table.appendChild(row);
    }
</script>

</body>
</html>