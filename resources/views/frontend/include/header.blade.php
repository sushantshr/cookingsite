<nav class="navbar fixed-top navbar-expand-lg navbar-custom" >
    <a class="navbar-brand" href="{{route('videopage')}}"><img src="/img/nepali.png" style="width:170px;height:51px;border-radius: 5px;"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            @if(Auth::check())
                <div class="dropdown" style="position:absolute;right:13px;top:4px;">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" >
                        <img src = "{{Auth::user()->avatar}}" width="40px" height="40px" style="border-radius: 20%;"> <span style="color:white;text-transform: capitalize;">{{Auth::user()->name}}</span></button>
                    <div class="dropdown-menu">
                        <li><a class="dropdown-item" href="{{ route('listinfo', Auth::id()) }}">{{ Auth::user()->name }}</a></li>
                        <li><a class="dropdown-item" href="{{ route('get.upload', Auth::id()) }}">Upload Video</a> </li>
                        <li><a class="dropdown-item" href="{{route('logout')}}">logout</a></li>
                    </div>
                </div>
            @else
                <ul class="nav navbar-nav " style="position: absolute;right:13px;top:19px;">
                    <li class="nav-item">
                        <a class="nav-link text-white " href="{{route('loginpage')}}"><span class="glyphicon glyphicon-log-in"></span><h6>Login</h6></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white" href="{{route('ad')}}"><span class="glyphicon glyphicon-user"></span><h6>Sign Up</h6></a>
                    </li>
                </ul>
            @endif
        </ul>
    </div>
</nav>
<style>
    .navbar-custom{
        background-color: #454545  ;
        color: #FFFFFF;
}
    a{
        color:#000000;
    }
    a:hover{
        color:#454545;
    }
</style>