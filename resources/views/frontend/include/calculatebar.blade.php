@if(Auth::check())

    <div style="width:0%;height:0%;left:0%;position:absolute;padding:0px;border-spacing: 0px;" id="mainpanel">
        <div  id="panel" style="background:whitesmoke;width:100%;position:absolute;display:none;left:95%;padding:0px 20px;" >
            <br>
                <form action="{{route('calculate')}}" id="calculateForm" method="post">

                    <label>No of people you are cooking to:</label>
                    <input type="number" name="people">
                    @if(isset($videos->id))
                        <input type ="hidden" name="video" value="{{ $videos->id }}" >
                    @endif
                    <hr>
                    <small>Please insert the table data in average</small>
                    <table id="table" class="table table-sm">
                        <tr>
                            <td>Ingredient</td>
                            <td>Quantity</td>
                            <td>unit</td>
                        </tr>
                        @if(isset($ingredient))
                            @foreach($ingredient as $ing)
                                <tr>
                                    <td><input type="text" name="ing[]" value="{{$ing['Ingredient']->name}}"></td>
                                    <td><input type="text" name="qty[]" value="{{ $ing['Ingredient']->qty }}"></td>
                                    <td>{{ $ing['Ingredient']->unit }}<input type="hidden" name="unit[]" value="{{ $ing['Ingredient']->unit }}"></td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                    <a class="btn" href="javascript:add()" style="background:#454545;border: hidden;color:white;">+</a>&nbsp;
                    <a class="btn" href="javascript:remove()"style="background:#454545;border: hidden;color:white;">-</a>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="submit" id="calculateIng" name="submit" value="Calculate" class="btn" style="background:#454545;border: hidden;color:white;" @if(!isset($ingredient)) disabled @endif>
                </form>
        </div>
    </div>
    <button class="btn btn-secondary" id="leftbutton" style=" font-size: 2.875rem; display: block;position:absolute;left:97%; padding:5px; "><b>&#10094;</b></button>
    <button class="btn btn-secondary" id="rightbutton" style=" font-size: 2.875rem;display: none;position:absolute;left:57%;padding:5px;">&#10095;</button>
@endif