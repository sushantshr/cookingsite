<!DOCTYPE html>
<html>
<head>
    <style>
        /** {*/
            /*box-sizing: border-box;*/
        /*}*/
        /*.header{*/
            /*background-color: grey;*/
            /*color: white;*/
            /*padding: 15px;*/
        /*}*/
        .column {
            float: left;
            padding: 15px;
        }
        /*.clearfix::after {*/
            /*content: "";*/
            /*clear: both;*/
            /*display: table;*/
        /*}*/
        /*.menu {*/
            /*width: 25%;*/
        /*}*/
        /*.content {*/
            /*width: 75%;*/
        /*}*/
        .menu ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            /*color:#000000;*/
        }
        .menu li {
            padding: 8px;
            margin-bottom: 8px;
            /*background-color: #33b5e5;*/
            /*color: #000000;*/
        }
        /*.menu li:hover {*/
            /*color: black;*/
        /*}*/
        a{
           color: black;
        }
        a:hover{
            color:#454545;
        }
    </style>
</head>
<body>

<div class="clearfix" style="position: fixed;width:213px; ">
    <div class="column menu">
        <ul>
            <li id="videopage"><a href="{{route('videopage')}}"><i class="material-icons">video_label</i><span style="position: absolute;">&nbsp;Videos</span></a></li>

            @if(auth::check())
                @if(!(Session('role')===null))
                    @if((Session('role')==0||Session('role')==1))
                        <li id="admin"><a href="{{ route('view.admin')  }}"><i class="material-icons">person</i><span style="position: absolute;">&nbsp;Admin</span></a></li>
                    @endif
                    @if((Session('role')!=0))
                        <li id="byanjan"><a href="{{ route('view.byanjan') }}"><i class="material-icons">personal_video</i><span style="position: absolute;">&nbsp;ByanjanVideos</span></a></li>
                        <li id="history"><a href="{{ route('get.history', Auth::user()->id) }}"><i class="material-icons">history</i><span style="position: absolute;">&nbsp;History</span></a></li>
                    @endif
                @else
                    <li>Please logout n login again</li>
                @endif
            @endif
            <li id="contact"><a href="{{route('contactpage')}}"><i class="material-icons">perm_contact_calendar</i><span style="position: absolute;">&nbsp;Contact</span></a></li>
            <li id="about"><a href="{{route('aboutpage')}}"><i class="material-icons">info_outline</i><span style="position: absolute">&nbsp;About</span></a></li>
        </ul>
        @if(!Auth::check())
            Want to Explore more? <a  href="{{route('loginpage')}}" style=""><b><u>Login</u></b></a> just to spice up your life
            @endif
    </div>
</div>


