@extends('frontend.layout.main')

@section('content')
    <table class="table table-striped">
        <thead class="thead-inverse">
        <tr>
            <th>ID</th>
            <th>Video Name</th>
            <th>Uploader</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($list as $s)
            <tr>
                <td>{{$s->id}}</td>
                <td><a href="{{ route('view.single',$s->id)}}">{{$s->name}}</a></td>
                <td><a href="{{ route('listinfo',$s->users->id)}}">{{$s->users->name}}</a></td>
                <td>
                    <a href="{{route('delete.video', $s->id)}}"><button class="btn btn-danger btn-raised" >Delete</button></a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection