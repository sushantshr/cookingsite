@extends('frontend.layout.main')

@section('content')
    <table class="table table-striped">
        <thead class="thead-inverse">
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
            @foreach($lists as $list)
                @if($list['role']!=0)
                <tr>
                    <td><a href="{{ route('listinfo', $list['id'])}}">{{$list['name']}}</a></td>
                    <td>{{$list['email']}}</td>
                    <td>{{$list['role']}}</td>
                    <td>
                        <a href="{{route('deactivate', $list['id'])}}">
                            <button class="btn btn-danger btn-raised" @if(Session('role')>=$list['role']) disabled @endif>Delete</button>
                        </a>
                       @if(Session('role')==0) <a class="btn btn-danger btn-raised" href = {{route('changerole', $list['id'])}}>Change role</a>@endif
                    </td>
                </tr>
                @endif
            @endforeach
        </tbody>
    </table>

@endsection