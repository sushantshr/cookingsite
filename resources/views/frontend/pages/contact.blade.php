@extends('frontend.layout.main')

@section('content')

    <div class="container">
        <div class="jumbotron" align="center" style="background-color: rgba(255, 255, 255, 0.5)">
            <h1>Contact</h1>
            <p>You can contact us at: byanjanapp@gmail.com</p>
        </div>
    </div>

    <h2 style="text-align: center;">Or</h2>

    <div class="row" >
        <div class="col-lg-3"></div>
        <div class="col-lg-6" style="background-color: rgba(255, 255, 255, 0.5);border-radius: 5px;">
            <form method="post" action="{{route('send.mail')}}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" name="name" placeholder="Your Name" @if(Auth::check()) value="{{ Auth::user()->name }}" @endif>
                </div>
                <div class="form-group">
                    <label>Email address</label>
                    <input type="email" class="form-control" name="mail" placeholder="name@example.com" @if(Auth::check()) value="{{ Auth::user()->email }}" @endif>
                </div>

                <div class="form-group">
                    <label>Message</label>
                    <textarea title="Your Message" class="form-control" id="exampleFormControlTextarea1" name="body" rows="3" placeholder="Your Message"></textarea>
                </div>
                <input type="submit" class="btn btn-secondary" value="Send Message">
            </form>
        </div>
    </div>
    <br>
    @endsection
@section('script')
    <script>
        $(function () {
            $('#contact').append("<i class=\"material-icons\" style='position: absolute;right: 15px;'>remove_red_eye</i>");
        })
    </script>
@endsection
