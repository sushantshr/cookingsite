@extends('frontend.layout.main')

@section('content')

    <div class="container">
        <form method="get" action="{{route('search.youtube')}}" >
            <div class="row">
                <input type="text" class="form-control" name="search" placeholder="Search Youtube" style="width:228px">
                <button type="submit" class="btn btn-default" style="padding-bottom:0px;"><i class="material-icons" style="font-size: 32px;">search</i></button>
            </div>
        </form>
    </div>
    {{ Session('msg') }}<hr>
    @if($content == null)
        <p> No search results </p>
    @else
        <div class="row">
        @foreach($content as $vid)
        <div class="col-lg-6">
            <div class="card" style="width: 20rem;">
                <img class="card-img-top" src="{{ $vid->snippet->thumbnails->medium->url }}" alt="Card image cap">
                <div class="card-body">
                    <h6 class="card-title">{{ $vid->snippet->title  }}</h6>
                    <a href="{{ route('watch.youtube', [$vid->id->videoId, $vid->snippet->title ])  }}" class="btn btn-primary">Watch</a>
                </div>
            </div>
        </div>
    @endforeach
        </div>
    @endif
    @endsection