@extends('frontend.layout.main')

@section('content')
    <h1>Sorry for the inconvenience.</h1>
    <h2>{{ $msg  }}</h2>
    <h3>Please go back to the page and reload</h3>
@endsection