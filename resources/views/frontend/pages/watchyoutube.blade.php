@extends('frontend.layout.main')

@section('content')
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="card col-lg-8">
            <div class="card-body">
                <h4 class="card-title">{{ $name }}</h4>

            <iframe class="card-img-bottom" src="https://www.youtube.com/embed/{{ $id }}" allowfullscreen width="580" height="360"></iframe>
            </div>
        </div>
    </div>
@endsection
