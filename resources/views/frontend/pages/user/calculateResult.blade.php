@extends('frontend.layout.main')

@section('content')
    @if($video!=null)
        <p><a href="{{ route('view.single',$video->id)}}">{{$video->name}}</a> </p>
        @endif
    <p>Ingredient for {{$people}} people</p>
    <table class="table">
        <tr class="thead-inverse">
            <th>Ingredient</th>
            <th>Quantity</th>
            <th>Unit</th>
        </tr>
    @foreach($quantity as $quan)
            <tr>
                <td>{{$quan['ing']}}</td>
                <td>{{$quan['qty']}}</td>
                <td>{{$quan['unit']}}</td>
            </tr>
    @endforeach
    </table>
    @endsection