@extends('frontend.layout.main')

@section('content')
    <div class="container">
        <form method="get" action="{{route('search.byanjan')}}" >
            <div class="row">
                <input type="text" class="form-control" name="search" placeholder="Search Byanjan" style="width:228px">
                <button type="submit" class="btn btn-default" style="padding-bottom:0px;"><i class="material-icons" style="font-size: 32px;">search</i></button>
            </div>
        </form>
    </div>
    <hr>
    <div class="row">
    @foreach($videos as $video)
        <div class="col-lg-4" style="padding-right:0px;padding-top: 7px;margin-bottom: 0px;">
            <div class="card">
                <img class="card-img-top" src="{{$video['img']}}" alt="{{ $video["name"] }}" style="max-width:364px;max-height:205px;" >
                <div class="card-body" style="padding-bottom: 0px;">
                    <h4 class="card-title" style="font-size: 17px;">{{ $video["name"] }}
                        @if($video['role']==2||$video['role']==0)
                            (Trusted By site)
                        @endif
                    </h4>
                    <a href="{{ route('listinfo', $video["uploader_id"]) }}">By: {{ $video["uploader"]  }}</a><br>
                    <a href="{{ route('view.single',$video["id"])}}" class="btn btn-primary">Watch</a>
                </div>
            </div>
        </div>
    @endforeach
    </div>
@endsection