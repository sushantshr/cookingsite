@extends('frontend.layout.inlayout')

@section('content')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>
  <form method="POST" action="{{route('editu', $getuser->id)}}" enctype="multipart/form-data">
        {{csrf_field()}}
      <div class="container">
          <h4>Mandatory</h4>
      </div>
      <div class="container">
          <div class="row">
              <div class="col-md-7">
                  <div class="jumbotron" style="padding: 30px 30px ;">

                    <div class="form-group">
                      <label for="exampleInputName1">Name</label>
                      <input type="text" name="name" class="form-control"  id="exampleInputName1" placeholder="Enter name" value="{{$getuser->name}}">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" @if($getuser->password==null) required @endif>
                    </div>

                  </div>
              </div>
          </div>
      </div>

      <div class="container">
          <h4>Optional</h4>
      </div>
      <div class="container">
          <div class="row">
              <div class="col-md-8">
                  <div class="jumbotron" style="padding: 30px 30px ;">

                      <div class="form-group">
                        <label for="exampleInputAvatar1">Avatar</label>
                          <div id="avatars">
                              <img src = "{{$getuser->avatar}}" width="80px" style="border-radius: 20px;"><br>
                              <input type="file" name="avatar">
                          </div>
                      </div>

                      <div class="form-group">
                          <label >About yourself</label>

                          <div>
                              <textarea name="about" id="comments" style="font-family:sans-serif;font-size:1.2em;" >{{ $getinfo->about  }}</textarea>
                          </div>
                      </div>

                      <div class="form-group">
                          <label>Interests on cooking field </label>
                          <div class="row">
                              @php $info = unserialize($getinfo->interest)
                              @endphp
                              @foreach($category as $cat)
                                  <div class="col-md-3">
                                    <input type="checkbox" style="margin-right:6px;" name="interest[]" @if(in_array( $cat->name, $info)) checked @endif value=" {{$cat->name}}">{{$cat->name}} &nbsp;
                                  </div>
                              @endforeach
                          </div>
                      </div>
                      <button type="submit" class="btn btn-primary" style="background: #454545;border: hidden;">Submit</button>
                  </div>
              </div>
          </div>
      </div>
    </form>
@endsection