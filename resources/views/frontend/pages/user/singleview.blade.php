@extends('frontend.layout.main')

@section('content')
    <br>
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <div class="card" style="width: 99%;">
                <video class="card-img-top" controls width="520" height="370" onplay="addcount()">
                    <source src="/{{$videos->url}}"> </source>
                </video>
                <div class="card-body"style="padding-bottom: 0px;">
                    <div class="card-title">
                       <h4> {{ $videos->name  }}</h4> <small> <span id="count">{{ $videos->view_count }}</span> Views</small>
                    </div>
                    <div class="card-text">
                        <p><a href="{{ route('listinfo', $user->id) }}"> By: {{ $user->name }}</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
<br>

    <div class="row " style="padding-bottom: 5px;">
        <div class="col-lg-2"></div>
        <div class="col-lg-8 card" style="padding-top: 20px;">
            <div class="row">
            <div class="col-lg-1"><img src="{{Auth::user()->avatar}}" style="width: 40px;height: 40px; border-radius:30px;" ></div>
            <div class="col-lg-11">
                <input type="text" name="comment" id="comment" style="width:90%;height:35px;border-radius: 5px; " placeholder="Write a comment . . . ">
                <button class="btn btn-info btn-sm" onclick="usercomment()" style="width:20%;">Add Comment</button>
            </div>
            </div>
        </div>
        <div class="col-lg-2"></div>
    </div>

    <div class="row" style="padding-bottom: 5px;">
        <div class="col-lg-2"></div>
        <div class="col-lg-8 card" id="viewcomment">
            @if(isset($comment))
            @foreach($comment as $com)
                <div style="padding-bottom: 10px;" class="row">
                    <div class="col-lg-1"><img src="{{ $com['user']->avatar }}" style="width: 40px;height: 40px; border-radius:30px;"></div>
                    <div class="col-lg-11" style="padding-top: 14px;">
                        <p style="margin-bottom: 0px;"><b>{{ $com['user']->name}}</b> {{ $com['comment'] }}</p>
                    </div>
                    <br>
                </div>
                @endforeach
            @endif
        </div>
    </div>

@endsection
@section('script')
    <script>
    function usercomment()
    {
        var id = "{{ $videos->id }}";
        var userid ="{{ Auth::user()->id }}";
        var coment = document.getElementById('comment').value;
        if(coment)
        {
            $.ajax({
            type: 'POST',
            url: '/api/add/comment',
            data:{
            video_id:id,
            user_id:userid,
            comment:coment
        },
            success: function (response){
            var commentdiv = document.createElement("div");
            console.log(response.user.avatar);
            commentdiv.innerHTML= "<div style='padding-bottom: 10px;' class='row'>" +
                                    "<div class='col-lg-1'><img src='" +
                                    response.user.avatar+"' style='width: 40px;height: 40px; border-radius:30px;'></div>" +
                                        "<div class='col-lg-11' style='padding-top: 14px;'>" +
                "<p style='margin-bottom: 0px;'><b>"+ response.user.name +"</b> "+ response.comment.comment +"</p>" + "</div>" +
                "<br>" + "</div>"+commentdiv.innerHTML;
           var commentArea =  document.getElementById('viewcomment');
            commentArea.insertBefore(commentdiv, commentArea.childNodes[0]);
            document.getElementById('comment').value="";
            }
        })
    }
    else
        return false;
    }
$flag = 0;
    function addcount()
    {
        if($flag ==0) {
            var id = "{{ $videos->id }}";
            $.ajax({
                type: 'GET',
                url: '/api/addcount/' + id,

                success: function (response) {

                    $('#count').text(response.data);
                    $flag = 1;
                }
            });
        }
    }
    </script>
    @endsection
