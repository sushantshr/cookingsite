@extends('frontend.layout.inlayout')

@section('content')
<br>
<div class="row container" >
    <div class="col-lg-1"></div>
    <div class="col-lg-2"> <img src="{{$getuser->avatar}}" width="100%"/> </div>
    <div class="col-lg-7">
        <p>{{$getuser->name}}</p>
        <p>{{$getuser->email}}</p>
        <p>@if($role->role==1)
                Viewer (In order to get a trusted label please email us!!)
            @else
                Trusted By the site
            @endif
        </p>
        <p>Interested in: @foreach(unserialize($getinfo->interest) as $info)
                {{$info}}
            @endforeach
        </p>
        <p>{!!$getinfo->about !!}</p>
    </div>
    <div class="col-lg-2">
        @if(Auth::user()->id==$getuser->id)
            <a href="{{ route('edit', $getuser->id) }}" ><button class="btn btn-default btn-raised">Edit</button></a><hr>
            <a href="{{ route('deactivate', $getuser->id) }}"><button class="btn btn-default btn-raised">Deactivate</button></a>
        @endif
    </div>
</div>
<br>
@if(count($video)>0)
<div class="container" >
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <div class="row" style="padding-left:90px;padding-top:18px;padding-right: 90px;padding-bottom: 5px;">
                    <div class="col-lg-4" >
                        <div class="card" >
                            <img class="d-block card-img-top" style="height:200px;max-width: 300px;"  src="{{ $video[0]->img }}" alt="{{ $video[0]->name }}">
                            <div class="card-body" style="padding-bottom: 0px;">
                                <p class="card-title">{{ $video[0]->name }}</p>
                                <a href="{{ route('view.single',$video[0]["id"])}}" class="btn btn-secondary">Watch</a>
                            </div>
                        </div>
                    </div>
                        @if(count($video)>1)
                    <div class="col-lg-4" >
                        <div class="card" >
                            <img class="d-block card-img-top" style="height:200px;max-width: 300px;"  src="{{ $video[1]->img }}" alt="{{ $video[1]->name }}">
                            <div class="card-body" style="padding-bottom: 0px;">
                                <p class="card-title">{{ $video[1]->name }}</p>
                                <a href="{{ route('view.single',$video[1]["id"])}}" class="btn btn-secondary">Watch</a>
                            </div>
                        </div>
                    </div>
                        @endif
                    @if(count($video)>2)
                        <div class="col-lg-4">
                            <div class="card" >
                                <img class="d-block card-img-top" style="height:200px;max-width: 300px;"  src="{{ $video[2]->img }}" alt="{{ $video[2]->name }}">
                                <div class="card-body" style="padding-bottom: 0px;">
                                    <p class="card-title">{{ $video[2]->name }}</p>
                                    <a href="{{ route('view.single',$video[2]["id"])}}" class="btn btn-secondary">Watch</a>
                                </div>
                            </div>
                        </div>
                    @endif
            </div>
            </div>
            @for($i=3;$i<(count($video));$i++)
                <div class="carousel-item">
                    <div class="row" style="padding-left:90px;padding-top:18px;padding-right: 90px;padding-bottom: 5px;">
                        <div class="col-lg-4">
                            <div class="card" >
                                <img class="d-block card-img-top" style="height:200px;max-width: 300px;"  src="{{ $video[$i]->img }}" alt="{{ $video[$i]->name }}">
                                <div class="card-body" style="padding-bottom: 0px;">
                                    <p class="card-title">{{ $video[$i]->name }}</p>
                                    <a href="{{ route('view.single',$video[$i]["id"])}}" class="btn btn-secondary">Watch</a>
                                </div>
                            </div>
                        </div>

                        @if(count($video)>($i+1))
                            <div class="col-lg-4">
                                <div class="card" >
                                    <img class="d-block card-img-top" style="height:200px;max-width: 300px;"  src="{{ $video[$i+1]->img }}" alt="{{ $video[$i+1]->name }}">
                                    <div class="card-body" style="padding-bottom: 0px;">
                                        <p class="card-title">{{ $video[$i+1]->name }}</p>
                                        <a href="{{ route('view.single',$video[$i+1]["id"])}}" class="btn btn-secondary">Watch</a>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if(count($video)>($i+2))
                            <div class="col-lg-4">
                                <div class="card" >
                                    <img class="d-block card-img-top" style="height:200px;max-width: 300px;"  src="{{ $video[$i+2]->img }}" alt="{{ $video[$i+2]->name }}">
                                    <div class="card-body" style="padding-bottom: 0px;">
                                        <p class="card-title">{{ $video[$i+2]->name }}</p>
                                        <a href="{{ route('view.single',$video[$i+2]["id"])}}" class="btn btn-secondary">Watch</a>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            @endfor
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev" style="width:50px;height:150px;background-color: #454545;border-radius: 100px;top:23%;">
            <span  class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next" style="width:50px;height:150px;background-color: #454545;border-radius: 100px;top:23%;">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
@endif
@endsection