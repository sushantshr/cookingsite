@extends('frontend.layout.main')

@section('content')
    <br>
    @if(isset($videos))
        @foreach($videos as $video)
                <div class="row">
                <div class="col-lg-3 ">
                    <img src="{{ $video['img'] }}" style="width:264px;height: 205px;" >
                </div>
                <div class="col-lg-4" style="padding-top:6%;background-color:rgba(255, 255, 255, 0.5);">
                    {{ $video['videoName'] }}<br>
                    By:{{ $video['uploader'] }}<br>
                    <a href="{{  route('view.single',$video["id"])}}" class="btn btn-primary"> Watch </a>
                </div>
                </div>
            <hr>
        @endforeach
    @else
        <p>You haven't watched any Byanjan videos till now</p>
    @endif
@endsection
@section('script')
    <script>
        $(function () {
            $('#history').append("<i class=\"material-icons\" style='position: absolute;right: 15px;'>remove_red_eye</i>");
        })
    </script>
@endsection

