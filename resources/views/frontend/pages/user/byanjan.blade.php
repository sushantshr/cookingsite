@extends('frontend.layout.main')

@section('content')
    <div class="container">
        <form method="get" action="{{route('search.byanjan')}}" >
            <div class="row">
                <input type="text" class="form-control" name="search" placeholder="Search Byanjan" style="width:228px">
                <button type="submit" class="btn btn-default" style="padding-bottom:0px;"><i class="material-icons" style="font-size: 32px;">search</i></button>
            </div>
        </form>
    </div>
    <hr>
    @if(isset($videos))
        @if($count!=0)
            <h4>Recommended Videos</h4><br>
            <div class="row">
                @endif
                @for($i=0;$i<$count;$i++)
                    <div class="col-lg-4" style="padding-right: 0px;padding-top: 7px;margin-bottom: 0px;">
                        <div class="card">
                        <img class="card-img-top" src="{{$videos[$i]['img']}}" alt="{{ $videos[$i]["name"] }}" style="max-width:364px;max-height:205px">
                        <div class="card-body" style="padding-bottom: 0px;">
                            <h4 class="card-title" style="font-size: 17px;">{{ $videos[$i]["name"] }}
                            @if($videos[$i]['role']==2||$videos[$i]['role']==0)
                                (Trusted By site)
                            @endif
                            </h4>
                            <a href="{{ route('listinfo', $videos[$i]["uploader_id"]) }}">By: {{ $videos[$i]["uploader"]  }}</a><br>
                            <a href="{{ route('view.single',$videos[$i]["id"])}}" class="btn btn-secondary" style="padding-top: 13px;">Watch</a>
                        </div>
                        </div>
                    </div>
                @endfor
                @if($count!=0)
            </div>
        @endif
        <hr>
        <h4>Byanjan Videos</h4><br>
        <div class="row">
            @for($j=$i;$j<count($videos);$j++)
                <div class="col-lg-4" style="padding-right: 0px;padding-top: 7px;">
                    <div class="card">
                    <img class="card-img-top" src="{{$videos[$j]['img']}}" alt="{{ $videos[$j]["name"] }}" style="max-width:364px;max-height:205px" >
                    <div class="card-body" style="padding-bottom: 0px;">
                        <h4 class="card-title" style="font-size: 17px;">{{ $videos[$j]["name"] }}
                        @if($videos[$j]['role']==2||$videos[$j]['role']==0)
                            (Trusted By site)
                        @endif
                        </h4>
                        <a href="{{ route('listinfo', $videos[$i]["uploader_id"]) }}">By: {{ $videos[$j]["uploader"]  }}</a><br>
                        <a href="{{ route('view.single',$videos[$j]["id"])}}" class="btn btn-primary">Watch</a>
                    </div>
                    </div>
                </div>
        @endfor
        </div>
    @else
        {{ "No videos" }}
    @endif
@endsection
@section('script')
    <script>
        $(function () {
            $('#byanjan').append("<i class=\"material-icons\" style='position: absolute;right:10px;'>remove_red_eye</i>");
        })
    </script>
@endsection