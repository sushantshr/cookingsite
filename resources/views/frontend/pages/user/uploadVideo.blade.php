@extends('frontend.layout.inlayout')

@section('content')
<form method="post" name="form" action="{{ route("post.upload") }}" enctype="multipart/form-data" >
    {{ csrf_field() }}

    <div class="row container">
    <div class="col-md-7">
    <div class="jumbotron" style="padding: 30px 30px ;">

    <div class="form-group">
        <label>Video Name &nbsp;</label>
            <input type="text" name="name" placeholder="Name of the video"  style="border-radius: 5px;" required>
    </div>
    <div class="form-group">
        <label >Select video &nbsp; </label>
        <label class="btn btn-secondary" style="background-color: #454545;"><input type="file" id="video" class="file_multi_video" name="video" size="50" required onchange="sizecheck()" style="display: none;">Select a file</label>
        <br>
        <video id="preview" style="width:215px;display:none;" controls>
            <source id="videosrc" src="">
        </video>
        <hr>
        <div class="row" >
            <div class="col-lg-6" id="rightPreview" style="display: none;border-right: 1px solid black;">
                <label>Image Preview</label>
                    <img id="canvasImg" style="width: 215px;height:120px;">
                <input type="hidden" id="defaultImg" name="defaultImg" value="">
                <canvas id="c" style="display:none;"></canvas>
            </div>
            <div class="col-lg-6" id="leftPreview" style="display: none;">
                <label class="btn btn-secondary btn-sm" style="background-color: #454545;"><input type="file" id="previewImg" name="videoImage" size="50" onchange="imagePreview()" style="display: none;">Select a preview Image</label>
                <img src="" id="previewImage" style="display:none;width:215px;height:120px;">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label>Select Category &nbsp;</label>
        <select name="category" style="border-radius:5px;height: 30px;">
            @foreach($category as $cat)
                <option name="{{$cat->name}}">{{$cat->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <table class="table table.striped" id="table">
            <thead class="thead-inverse">
            <tr>
                <th >Ingredient</th>
                <th >Quantity</th>
                <th >Unit</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <input type="hidden" value="{{ $id  }}" name="id">

        <a class="btn btn-primary btn-sm" href="javascript:add()" style="background: #454545;border: hidden;">+</a>&nbsp;
        <a class="btn btn-danger btn-sm" href="javascript:remove()" style="background: #454545;border: hidden;">-</a>
        <br>
            <small>Please we require you to give the ingredient lists to our site so that users can easily calculate the required amounts</small>
        <hr>
        <input type="submit" name="submit" value="Upload video" class="btn btn-default" style="background: #454545;border: hidden;color: white">


     <script type="text/javascript">

         document.getElementById("preview").onloadeddata = function(){
             var canvas = document.getElementById("c");
             var video = document.getElementById("preview");
             canvas.width = video.videoWidth;
             canvas.height =  video.videoHeight;
             canvas.getContext('2d').drawImage(video, 0, 0);
             var can = document.getElementById("c");
             console.log(can);
             var src = can.toDataURL('image/png');
             document.getElementById('defaultImg').value = src;
             document.getElementById('canvasImg').src = src;
         };
         window.onload = add();

         function imagePreview()
         {
             var preview = document.getElementById('previewImg');
             var image = document.getElementById('previewImage');
             image.style.display = "block";
             image.src=URL.createObjectURL(preview.files[0]);
             console.log(image, document.getElementById('video').files);
         }
         function sizecheck()
         {
            var x = document.getElementById('video');
            console.log(x.files[0], Math.ceil(x.files[0].size/1024/1024));
            var file_size = Math.ceil(x.files[0].size/1024/1024);
            var type = x.files[0].type;
             if(file_size>77)
             {
                     alert("File size too large");
                 document.getElementById('video').value = "";

             }
             else {
                 $(document).on("change", "#video", function (evt) {
                     var $source = $('#videosrc');
                     $('#preview').css({
                        display:'block'
                     });
                     $source[0].src = URL.createObjectURL(this.files[0]);
                     $source.parent()[0].load();
                     $('#rightPreview').css(
                         {
                             display:'block'
                         }
                     );
                     $('#leftPreview').css(
                         {
                             display:'block'
                         }
                     )
                 });

             }
         }

         function remove()
         {
             $last = document.getElementById('table').childNodes[3];
             if($last.children.length>1)
             {
                 $child = $last.lastChild;
                 $last.removeChild($child);
             }
         }

         function add() {
             var row = document.createElement("tr");
             var input1 = document.createElement("input");
             input1.required = true;
             input1.type = "text";
             input1.style.width= "100%";
             input1.placeholder = "Ingredient name ....";

             var input2 = document.createElement("input");
             input2.required = true;
             input2.type = "text";
             input2.placeholder = "quantity here ....";

             var ing = document.createElement("td");
             input1.name = "ing[]";
             ing.appendChild(input1);

             var qty = document.createElement("td");
             input2.name = "qty[]";
             input2.style.width= "100%";
             input2.onchange= function ()
             {
                 var val = this.value;
                 if(isNaN(val))
                 {
                     this.value = 0;
                     alert("must input number");
                 }
             };
             qty.appendChild(input2);

             var selecttd = document.createElement("td");
             var select = document.createElement("select");
             select.name="unit[]";
             select.style.width= "100%";
             var option = [];

             var i=0;
             var unit = ["kilogram", "gram", "pound", "litre", "millilitre","teaspoon","tablespoon", "metre", "centimetre", "millimeter","cup", "pieces", "pinch", "unit"];
             for (i = 0; i < unit.length; i=i+1)
             {
                 option[i] = document.createElement("option");
                 option[i].value=unit[i];
                 option[i].innerHTML = unit[i];
                 select.appendChild(option[i]);
            }
            selecttd.appendChild(select);
             var table = document.getElementById("table").childNodes[3];

             row.appendChild(ing);
             row.appendChild(qty);
             row.appendChild(selecttd);
              console.log(table.childNodes[3]);
             table.appendChild(row);
         }

     </script>
    </div>
    </div>
    <div class="col-md-5">
        <img src="/img/byanjan0.png"  width="110%" style="padding-top: 47px;">
    </div>
    </div>
</form>

    @endsection