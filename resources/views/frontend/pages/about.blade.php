@extends('frontend.layout.main')

@section('content')

    <div class="container">
        <div class="jumbotron">
            <br>
            <h3 align="center"> Cooking smarter. Together. </h3>
            <p align="center" class="jumbotron"> <b> Byanjan is a simple website for finding and discovering new receipes. We are a group consisting of 4 individuals passionate about desigining and developing of web applications. For more inquiries visit us at </b> <a href="https://www.facebook.com/ByanjanApp/" target="_blank"> <u>Facebook </u></a></b> </p>
        </div>
    </div>

@endsection
@section('script')
    <script>
        $(function () {
            $('#about').append("<i class=\"material-icons\" style='position: absolute;right: 15px;'>remove_red_eye</i>");
        })
    </script>
@endsection
