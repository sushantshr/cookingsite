@extends('frontend.layout.inlayout')

@section('content')
<form method="POST" action="{{route('addu')}}">
    {{csrf_field()}}
    <div class="container">
        <h4>Mandatory</h4>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="jumbotron" style="padding: 30px 30px ;">

                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control" placeholder="Enter name" required>
                    </div>

                    <div class="form-group">
                        <label >Email address</label>
                        <input type="email" name="email" class="form-control" placeholder="Enter email" required>
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>

                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control" placeholder="Password" required>
                    </div>

                 </div>
            </div>
        </div>
    </div>

    <div class="container">
        <h4>Optional</h4>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="jumbotron" style="padding: 30px 30px ;">

                    <div class="form-group">
                        <label for="exampleInputAvatar1">Avatar</label>
                        <div id="avatars">

                            <label class="avatars">
                                <input type="radio" name="avatar" value="/avatarimg/m.jpg" />
                                <img src="/avatarimg/m.jpg" height="100px" width="100px" alt="male"/>
                            </label>

                            <label class="avatars">
                                <input type="radio" name="avatar" value="/avatarimg/f.jpg"/>
                                <img src="/avatarimg/f.jpg" height="100px" width="100px" alt="female"/>
                            </label>
                            <br>
                            <label><small>Note: You will be able to change the avatars once you log in</small></label>
                         </div>
                    </div>

                    <div class="form-group">
                        <label >About yourself</label>
                        <div>
                                <textarea name="about" id="comments" style="font-family:sans-serif;font-size:1.2em;" width="100px" placeholder="Write about yourself..."></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Interests on cooking field</label>
                        <div class="row">
                        @foreach($category as $cat)
                            <div class="col-md-3">
                                <input type="checkbox" name="interest[]" value="{{$cat->name}}" style="margin-right:6px;">{{$cat->name}} &nbsp;
                            </div>
                        @endforeach
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary" style="background: #454545;border: hidden;">Submit</button>

                </div>
            </div>
        </div>
    </div>
</form>
@endsection