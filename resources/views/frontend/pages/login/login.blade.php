@extends('frontend.layout.inlayout')

@section('content')

    <div class="container">
        <h4>Login</h4>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="jumbotron" style="padding: 20px 20px;">
                    <form method="POST" action="{{route('logg')}}">
                        {{csrf_field()}}
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" required>
                                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password</label>
                                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" >
                                </div>
                                <button type="submit" class="btn btn-primary" style="background: #454545;border: hidden;">Submit</button>
                    </form>

                </div>
            </div>
            <div class="col-md-6">
                <img src="/img/byanjan0.png" style="width:437px;height: 271px;" >
            </div>
        </div>
    </div>

    <div class="container">
        <h5>Or Login With</h5>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="jumbotron" style="padding: 10px 10px;">

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <a href="{{ url('/auth/google') }}" class="btn btn-google" ><img src="/img/google.png" style="width: 50px;"></a>
                            <a href="{{ url('/auth/facebook') }}" class="btn btn-facebook"><img src="/img/facebook.png" style="width: 50px;"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        a:hover{
            color:#454545;
        }
    </style>

@endsection