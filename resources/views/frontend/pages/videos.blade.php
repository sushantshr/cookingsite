@extends('frontend.layout.main')

@section('content')
    <div class="container">
        <form method="get" action="{{route('search.youtube')}}" >
            <div class="row">
                <input type="text" class="form-control" name="search" placeholder="Search Youtube" style="width:228px">
                <button type="submit" class="btn btn-default" style="padding-bottom:0px;"><i class="material-icons" style="font-size: 32px;">search</i></button>
            </div>
        </form>
    </div>
        @foreach($datas as $data)
        <h3>{{$data['channel']}}</h3>
        <div class="row">
            @foreach($data['images'] as $image)
            <div class="col-lg-4 card" style="padding-right: 0px;padding-top: 7px;">
                <img class="card-img-top" src="{{ $image['image'] }}" alt="{{ $image['name'] }}">
                <div class="card-body" style="padding-bottom: 0px;">
                    <h4 class="card-title" style="font-size: 17px;">{{ $image['name'] }}</h4>
                    <a href="{{ route('watch.youtube', [$image["id"], $image['name']]) }}" class="btn btn-primary">Watch</a>
                </div>
            </div>
            @endforeach
            <div class="card-body">
                <a href="{{ route('watchmore', $data['id'])  }}">View More</a>
            </div>
        </div>
        @endforeach
    <div class="row">
        <div class="col-2"></div>
        <div class="col-lg-8" style="background-color:#ffffff;text-align:center;">
            @for($i=0;$i<ceil($total/10);$i++)
                <a href="{{ route('videopage', $i)  }}" class="btn btn-primary">{{$i+1}}</a>
            @endfor
        </div>
        <div class="col-2"></div>
    </div>
    <br>
@endsection

@section('script')
    <script>
        $(function () {
            $('#videopage').append("<i class=\"material-icons\" style='position: absolute;right: 15px;'>remove_red_eye</i>");
        })
    </script>
@endsection