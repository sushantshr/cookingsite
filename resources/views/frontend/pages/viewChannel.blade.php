@extends('frontend.layout.main')

@section('content')
    <br>

@for($i=0;$i<sizeof($data);$i+=2)
    <div class="row" >
        <div class="col-lg-1"></div>
        <div class="col-lg-4">
            <div class=" card" style="width:22rem;height:287px;">
                <img class="card-img-top" src="{{ $data[$i]['image'] }}" alt="Card image cap">
                <div class="card-body" style="padding-bottom: 0px;">
                    <p style="margin: 0px;">{{ $data[$i]['name']  }}</p>
                    <a href="{{ route('watch.youtube', [$data[$i]['id'],$data[$i]['name']])  }}" class="btn btn-primary">Watch</a>
                </div>
            </div>
        </div>
        <div class="col-lg-1"></div>
        @if(sizeof($data)>($i+1))
            <div class="col-lg-4">
                <div class=" card" style="width:22rem;height:287px;">
                    <img class="card-img-top" src="{{ $data[$i+1]['image'] }}" alt="Card image cap">
                    <div class="card-body" style="padding-bottom: 0px;">
                        <p style="margin: 0px;">{{ $data[$i+1]['name']}}</p>
                        <a href="{{ route('watch.youtube', [$data[$i+1]['id'],$data[$i+1]['name']])  }}" class="btn btn-primary">Watch</a>
                </div>
            </div>
            </div>
        @endif
    </div>
<hr>
@endfor
    <div class="row">
        <div class="col-2"></div>
        <div class="col-lg-8" style="background-color:#ffffff;text-align:center;">
            @for($i=1;$i<9;$i++)
                <a href="{{ route('watchmore', [$id, $i]) }}" class="btn btn-primary"> {{ $i }}</a>
            @endfor
        </div>
        <div class="col-2"></div>
    </div>
@endsection

