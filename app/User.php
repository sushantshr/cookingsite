<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'password', 'email', 'provider', 'provider_id', 'avatar', 'status_id', 'remember_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public $timestamps = false;

    public function status()
    {
        return $this->belongsTo('App\Status', 'status_id', 'id');
    }

    public function comment()
    {
        return $this->hasMany('App\Comment', 'user_id', 'id');
    }

    public function recipie()
    {
        return $this->hasMany('App\Recipie', 'user_id', 'id');
    }

    public function userDetail()
    {
        return $this->hasMany('App\UserDetail', 'user_id', 'id');
    }

    public function video()
    {
        return $this->hasMany('App\Video', 'user_id', 'id');
    }
}
