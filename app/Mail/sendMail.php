<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class sendMail extends Mailable
{
    use Queueable, SerializesModels;
    private $name;
    private $body;
    private $mail;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $name, $body, $mail)
    {
        $this->name = $name;
        $this->body = $body;
        $this->mail = $mail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('frontend.emails.mail')->with(['name'=>$this->name, 'body'=>$this->body, 'mail'=>$this->mail]);
    }
}
