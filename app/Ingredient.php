<?php namespace App;

/**
 * Eloquent class to describe the ingredient table
 *
 * automatically generated by ModelGenerator.php
 */
class Ingredient extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'ingredient';

    public $timestamps = false;

    protected $fillable = array('name', 'qty', 'unit');

    public function recipie()
    {
        return $this->hasMany('App\Recipie', 'ingredient_id', 'id');
    }
}
