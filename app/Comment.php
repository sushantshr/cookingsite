<?php namespace App;

/**
 * Eloquent class to describe the comment table
 *
 * automatically generated by ModelGenerator.php
 */
class Comment extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'comment';

    public $timestamps = false;

    protected $fillable = array('comment', 'video_id', 'user_id');

    public function users()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function status()
    {
        return $this->belongsTo('App\Status', 'status_id', 'id');
    }

    public function video()
    {
        return $this->belongsTo('App\Video', 'video_id', 'id');
    }
}
