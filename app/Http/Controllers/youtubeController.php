<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use App\Video;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use GuzzleHttp\Client as GuzzleHttpClient;
use App\channel;
use Mockery\Exception;

class youtubeController extends Controller
{
    //
    public $client;

    public function __construct()
    {
        $this->client = new GuzzleHttpClient();
    }


    public function getChannel($id, $page=0)
    {
        $url = 'https://www.googleapis.com/youtube/v3/search?key=AIzaSyDLmgFjGfkp0yVgAI8K914ISbxWL4h1Xpk&part=snippet,id&order=date&maxResults=12&type=video&channelId='.$id;

        try{
        $apiRequest = $this->client->request('GET', $url);

        $content = json_decode($apiRequest->getBody()->getContents());

        if($page!=0)
        {
            $nextpage = $content->nextPageToken;
            for($i=0;$i<$page;$i++)
            {
                $apiRequest = $this->client->request('GET', $url.'&pageToken='.$nextpage);
                $content = json_decode($apiRequest->getBody()->getContents());
                $nextpage = $content->nextPageToken;
            }
        }
        $i = 0;
        foreach ($content->items as $items) {

            $data[$i] = [
                'image' => $items->snippet->thumbnails->medium->url,
                'id' => $items->id->videoId,
                'name' => $items->snippet->title
            ];
            $i++;
        }
        }
        catch(\Exception $e){
            $msg = "Sorry one of the server is down";
            return view('frontend.pages.error', compact('msg'));
        }
        return view('frontend.pages.viewChannel', compact('data', 'id') );
    }

    public function listChannel($page=0)
    {

        $total = channel::count();
        $j=0;
        $up=$page*10+1;
        $down =$page*10+10;
        if($down>$total)
            $down = $total;
        try {
            for ($k=$up;$k<=$down;$k++) {
                $channel = channel::where('id', $k)->get()->first();
                if(count($channel)) {
                    $channelName = $channel->name;
                    $channelId = $channel->channel_id;
                    try {
                        $url = 'https://www.googleapis.com/youtube/v3/search?key=AIzaSyDLmgFjGfkp0yVgAI8K914ISbxWL4h1Xpk&part=snippet,id&maxResults=3&order=date&type=video&channelId=' . $channelId;
                        $request = $this->client->request('get', $url);
                        $apirequest = $this->client->request('get', $url);
                        $content = json_decode($apirequest->getBody()->getContents());
                        $i = 0;
                        foreach ($content->items as $items) {
                            $images[$i] = ['image' => $items->snippet->thumbnails->medium->url,
                                'id' => $items->id->videoId,
                                'name' => $items->snippet->title
                            ];
                            $i++;
                        }
                        $datas[$j] = [
                            "id" => $channelId,
                            "channel" => $channelName,
                            "images" => $images
                        ];
                        $j++;
                    }
                    catch(\Exception $e){
                        $msg = "Sorry one of the server is down";
                        return view('frontend.pages.error', compact('msg'));
                    }


                }
            }
        }
            catch(Exception $e)
            {
                return redirect()->route('error', 'Sorry the server is experiencing some error please try to reload');
            }

        return view('frontend.pages.videos', compact('datas', 'total'));
    }

    public function searchYoutube(Request $request)
    {
        $search = $request->input('search');
        $url = 'https://www.googleapis.com/youtube/v3/search?key=AIzaSyDLmgFjGfkp0yVgAI8K914ISbxWL4h1Xpk&part=snippet,id&order=relevance&maxResults=13&type=video&topicId=/m/02wbm&videoCaption=closedCaption&q='.$search;
        try {
        $apirequest = $this->client->request('get', $url);
        $content = json_decode($apirequest->getBody()->getContents())->items;

        }
        catch(\Exception $e){
            $msg = "Sorry one of the server is down";
            return view('frontend.pages.error', compact('msg'));
        }
        return view('frontend.pages.searchResult', compact('content'));
    }

    public function searchByanjan(Request $request)
    {
        $search = $request->input('search');
        $result = Video::where('name','like',  "%$search%")->get();

        if(count($result))
        {
            $i=0;
            foreach($result as $vid)
            {
                $user=User::find($vid->user_id);
                $role = Role::where('user_id', $user->id)->get()->first();
                $videos[$i] =[
                    "id"=>$vid->id,
                    "name"=>$vid->name,
                    'img'=>$vid->img,
                    "role"=>$role->role,
                    "uploader"=>$user->name,
                    "uploader_id"=>$user->id,
                ];

                $i++;
            }
            return view('frontend.pages.user.searchByanjan', compact('videos'));
        }
        else
        {
            Session(['msg'=>"Sorry our database has no video related to $search, here are some youtube videos related to the search "]);
            return $this->searchYoutube($request);
        }
    }


}
