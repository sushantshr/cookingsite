<?php

namespace App\Http\Controllers;

use App\Role;
use App\UserDetail;
use Illuminate\Http\Request;
use Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Mockery\Exception;
use Socialite;

class logincontroller extends Controller
{
    //
    function index()
    {
        return view('frontend.pages.login.login');
    }

    function login1(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');

        if (Auth::attempt(['email' => $email, 'password' => $password])) {

            $role = Role::where('user_id', Auth::user()->id)->get()->first();
            Session(['role'=>$role->role]);
            if($role->role==0||$role->role==2) {
                return redirect()->route('view.admin');
            }
            else {
                return redirect()->route('videopage');
            }
        } else {
            return redirect()->route('loginpage');
        }

    }

    function logout()
    {
        Auth::logout();
        Session(['role'=>1]);
        return redirect()->route('videopage');
    }

    public function redirectToProvider($provider)
    {
        try {
            return Socialite::driver($provider)->redirect();
        } catch (Exception $e) {
            return redirect()->route('error', "The $provider provider is currently expreiencing some error please try again later!");
        }
    }

    public function handleProviderCallback($provider)
    {
        try {
            $user = Socialite::driver($provider)->user();
            $authUser = $this->findOrCreateUser($user, $provider);
            if($authUser==false) {
                $msg = "Sorry the user name associated with the $provider is already set in the database";
                return view("frontend.pages.error", compact('msg'));
            }
            Auth::login($authUser, true);
            $role = Role::where('user_id', Auth::user()->id)->get()->first();

            Session(['role'=>$role->role]);
            if ($authUser->password == null) {
                return redirect()->route('edit', $authUser->id);
            } else {
                return redirect()->route('videopage');
            }
        } catch (Exception $e) {
            return redirect()->route('error', "sorry the login servers are down");
        }

    }

    public function findOrCreateUser($user, $provider)
    {

        $authUser = User::where('provider_id', $user->id)->first();
        if ($authUser) {
            return $authUser;
        }

        $email = User::where('email', $user->email)->first();

        if($email!=null) {
            if ($email->email == $user->email)
                return false;
        }
            $userdetail = User::create([
                'name' => $user->name,
                'email' => $user->email,
                'provider' => $provider,
                'avatar' => $user->avatar_original,
                'provider_id' => $user->id,
                'status_id' => 3
            ]);
            $interest = serialize(array("Breakfast"));
            UserDetail::create([
                'about' => 'Me!!!',
                'interest' => $interest,
                'user_id' => $userdetail->id
            ]);

            Role::create([
                'user_id' => $userdetail->id
            ]);
            return $userdetail;

    }
}