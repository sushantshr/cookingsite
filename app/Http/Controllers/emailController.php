<?php

namespace App\Http\Controllers;

use App\Mail\sendMail;
use Illuminate\Http\Request;
use Mail;

class emailController extends Controller
{
    //
    public function sendMail(Request $request)
    {
        $mail = $request->input('mail');
        $name = $request->input('name');
        $body = $request->input('body');
        Mail::to('sushantshr01@gmail.com')->send(new sendMail($name, $body, $mail));

        return redirect()->route('videopage');
    }
}
