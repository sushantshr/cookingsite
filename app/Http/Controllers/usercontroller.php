<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Ingredient;
use App\userlog;
use App\VideoIngredient;
use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\UserDetail;
use App\Video;
use Auth;
use Storage;

class usercontroller extends Controller
{
    public function adduser(Request $request)
    {
        //mandatory form inputs
        $name=$request->input('name');
        $email=$request->input('email');
        $password=bcrypt($request->input('password'));
        //optional form inputs
        $about=$request->input('about');
        $interest=$request->input('interest');
        $avatar = $request->input('avatar');
        //give default value
        if($avatar==null)
        {
            $avatar = '/avatarimg/m.jpg';
        }
        if($about=='')
        {
            $about = "Meee!!!";
        }
        if($interest==null)
        {
            $interest = array('Breakfast');
        }

        $add=User::create([
            'name'=>$name,
            'email'=>$email,
            'password'=>$password,
            'provider'=>'cookingsite',
            'provider_id'=>'001',
            'avatar'=>$avatar,
            'status_id'=>3
        ]);
        $user_id = $add->id;

        //input the optional user data
        $addInfo=UserDetail::create([
            'about'=>$about,
            'interest'=>serialize($interest),
            'user_id'=>$user_id
        ]);

        //input the role of the user
        $role=Role::create([
            'role'=>1,
            'user_id'=>$user_id
        ]);

        return redirect()->route('videopage');
    }

    public function deleteuser($id)
    {
        $del=User::find($id)->delete();
        $detail=UserDetail::where('id', $id)->delete();
        $role=Role::where('id', $id)->delete();
        $log = userlog::where('user_id', $id)->delete();
        $video = Video::where('user_id', $id)->get();

        foreach($video as $vid)
        {
            $this->deleteVideo($vid->id);
        }
        if(Auth::user()->id==$id) {
            Auth::logout();
            return redirect()->route('videopage');
        }
        else
            return redirect()->route('list.users');
    }

    public function getoneuserdetail($id)
    {
        $getuser = User::where('id', $id)->get()->first();
        $getinfo = UserDetail::where('user_id', $id)->get()[0];
        $role = Role::where('user_id',$id)->get()->first();
        $video = Video::where('user_id', $id)->get();
        return view('frontend.pages.user.listinfo', compact('getuser', 'getinfo', 'role', 'video'));
    }

    public function edituserdetail($id)
    {
        $getuser = User::find($id);
        $getinfo = UserDetail::where('user_id', $id)->get()->first();
        $category = Category::all();

        return view('frontend.pages.user.edit', compact('getuser', 'getinfo', 'category'));
    }

    public function edituser($id, Request $request)
    {
        //mandatory form inputs
        $name=$request->input('name');
        $password=$request->input('password');
        //optional form inputs
        $about=$request->input('about');
        $interest=serialize($request->input('interest'));

        if($_FILES["avatar"]["size"]!=0)
        {
            $target_dir = "avatarimg/";
            $ext = explode('.' , $_FILES["avatar"]["name"]);
            $target_file = $target_dir . microtime().".".end($ext);
            move_uploaded_file($_FILES["avatar"]["tmp_name"], $target_file);

            $add=User::find($id)->update([
                'avatar'=>'/'.$target_file
            ]);
        }
        if($password=="") {
            $add = User::find($id)->update([
                'name' => $name,
            ]);
        }
        else
        {
            $add = User::find($id)->update([
                'name' => $name,
                'password'=>bcrypt($password),
            ]);
        }

        $editinfo=UserDetail::where('user_id', $id)->update([
            'about'=>$about,
            'interest'=>$interest
        ]);
        $role = Role::where('user_id', $id)->get()->first();
        Session(['role'=>$role->role]);
        return redirect()->route('listinfo', $id);
    }

    public function edituserinfo($id, Request $request)
    {
        $about=$request->input('about');
        $interest=$request->input('interest');
        $user_id=$request->input('user_id');

        $editInfo=UserDetail::find($id)->update([
            'about'=>$about,
            'interest'=>$interest,
            'user_id'=>$user_id
        ]);
        return redirect()->route('listuserinfo');
    }

    public function uploadVideoInfo($id)
    {
        $category = Category::all();
        return view('frontend.pages.user.uploadVideo', compact('category', 'id'));
    }

    public function uploadVideo(Request $request)
    {
        if($_FILES['videoImage']['size']!=0)
        {
            $target_dir = "uploads/";
            $ext = explode('.' , $_FILES["videoImage"]["name"]);
            $src = $target_dir . microtime().".".end($ext);
            move_uploaded_file($_FILES["videoImage"]["tmp_name"], $src);
        }
        else{
            $img = $request->input('defaultImg');
            $img = str_replace('data:image/png;base64,', '', $img);
            $img = str_replace(' ', '+', $img);
            $fileData = base64_decode($img);
            //saving
            $target_dir = "uploads/";
            $src = $target_dir.microtime().".png";
            file_put_contents($src, $fileData);
        }
        $name = $request->input('name');
        $category = $request->input('category');
        $ingredient = $request->input('ing');
        $qty = $request->input('qty');
        $id = $request->input('id');
        $unit = $request->input('unit');

        $catId = Category::where('name', $category)->get()[0]->id;
        $target_dir = "uploads/";
        $ext = explode('.' , $_FILES["video"]["name"]);

        $target_file = $target_dir . microtime().".".end($ext);

        $vid = Video::create([
            'name'=>$name,
            'url'=>$target_file,
            'category_id'=>$catId,
            'user_id'=>$id,
            'img'=>"/".$src
        ]);
        $i=0;
        if($vid) {
            move_uploaded_file($_FILES["video"]["tmp_name"], $target_file);
            foreach ($ingredient as $ing) {
                $addIng = Ingredient::create([
                    'name' => $ing,
                    'qty' => $qty[$i],
                    'unit' =>$unit[$i]
                ]);


                VideoIngredient::create([
                    'video_id' => $vid->id,
                    'ingredient_id' => $addIng->id
                ]);
                $i++;
            }
            $request->session()->flash("msg","File Uploaded");
            return redirect()->route('listinfo', $id);
        }
        else
        {
            $request->session()->flash("msg","Sorry, Error on file upload");
            return redirect()->route('upload/video', $id);
        }
    }

    public function viewByanjan()
    {
        $video = $this->getRecommendations();
        $i=0;
        if(empty($video)) {

            return view('frontend.pages.user.byanjan');
        }

        foreach($video["video"] as $vid)
        {
            $user=User::find($vid->user_id);
            $role = Role::where('user_id', $user->id)->get()->first();
            $videos[$i] =[
                "id"=>$vid->id,
                "name"=>$vid->name,
                'img'=>$vid->img,
                "role"=>$role->role,
                "uploader"=>$user->name,
                "uploader_id"=>$user->id,
            ];

            $i++;
        }
        $count = $video["count"];
        return view('frontend.pages.user.byanjan', compact('videos','count' ));
    }

    public function getRecommendations()
    {
        $recentUser = User::find(Auth::user()->id);
        $recentdetail = UserDetail::where('user_id',$recentUser->id )->get()->first();
        $categories = unserialize($recentdetail->interest);
        $i=0;
        $videos = Video::all();
        $interestCat = null;
        foreach($categories as $category)
        {
            $interestCat[$i]= Category::where('name', $category)->get()->first()->id;
            $i++;
        }
        $log = userlog::where('user_id', Auth::user()->id)->get()->first();

        if(!empty($log)) {
            $logcatid = unserialize($log->log);
            $i = 0;
            $logCat = null;
            foreach ($logcatid as $logcat) {
                if (count(Video::where('id', $logcat)->get()) != 0) {
                    $vid = Video::where('id', $logcat)->get()->first();
                    if(!file_exists($vid->url))
                    {
                        $pos = array_search($logcat, $logcatid);
                        array_splice($logcatid, $pos, 1);
                        userlog::where('user_id', (Auth::user()->id))->update([
                            'log'=>serialize($logcatid)
                        ]);
                        $this->viewByanjan();
                    }
                    $logCat[$i] = Video::find($logcat)->category->id;
                    $i++;
                }
            }
            if ($logCat != null && $interestCat != null)
            {
                $result = array_intersect($logCat, $interestCat);
                $i = 0;
                $j = 0;
                foreach ($videos as $video) {
                    if (in_array($video->category_id, $result)) {
                        $priorityList[$i] = $video;
                        $i++;
                    } else {
                        $videoList[$j] = $video;
                        $j++;
                    }
                }
            }
            else
            {
                $j = 0;
                foreach ($videos as $video)
                {
                    $videoList[$j] = $video;
                    $j++;
                }
            }
            if(!empty($videoList)&&!empty($priorityList))
                $list = ["video"=>array_merge($priorityList, $videoList), "count"=>$i];
            elseif (empty($videoList)&&(!empty($priorityList)))
                $list = ["video"=> $priorityList, "count"=>$i];
            else
                $list = ["video"=> $videoList, "count"=>0];

        }
        else{
            $list = ["video"=>$videos, "count"=>0];
        }
         return $list;
    }

    public function viewSingle($id)
    {
        $videos = Video::find($id);

        $comments = Comment::where('video_id', $id)->orderBy('id', 'desc')->get();
        $i=0;
        $log = userlog::where('user_id', Auth::user()->id)->get()->first();
        if(empty($log))
        {
            $userLog = array("$id");
            userlog::where('user_id', Auth::user()->id)->create([
                'user_id'=>Auth::user()->id,
                'log'=>serialize($userLog)
            ]);
        }
        else
        {
            $logtillnow = unserialize($log->log);
            if(!in_array($id, $logtillnow ))
            {
                $userlog = array_push($logtillnow, $id);
                userlog::where('user_id', Auth::user()->id)->update([
                    'log'=>serialize($logtillnow)
                ]);
            }
        }
        $i=0;
        foreach($comments as $com)
        {
            $user = User::where('id', $com->user_id)->get()->first();
            $comment[$i]=[
              "comment"=>$com->comment,
              "user"=>$user
            ];
            $i++;
        }

        $user = Video::find($id)->users()->where('id', $videos->user_id)->first();
        $ingredient = VideoIngredient::with('Ingredient')->with('video')->where('video_id', $id)->get();

        return view('frontend.pages.user.singleview', compact('videos', 'comment', 'ingredient','user'));
    }

    public function deleteVideo($id)
    {
        $comments = Comment::where('video_id', $id)->delete();
        $viding = VideoIngredient::where('video_id', $id)->get();
        foreach($viding as $ing)
        {
            Ingredient::find($ing->ingredient_id)->delete();
        }
        VideoIngredient::where('video_id',$id )->delete();
        $file = Video::find($id)->get()->first();
        if(file_exists($file->url))
        {
            unlink($file->url);
        }
        Video::find($id)->delete();

        return redirect()->route('list.video');
    }
    public function addComment(Request $request)
    {
        $comment = $request->input('comment');
        $videoid = $request->input('video_id');
        $userid = $request->input('user_id');

        $getComment = Comment::create([
            'comment'=>$comment,
            'user_id'=>$userid,
            'video_id'=>$videoid
        ]);
        $user = User::find($userid);
        return response()->json(['comment'=>$getComment, 'user'=>$user]);
    }

    public function addcount($id)
    {
        $videoinfo = Video::where('id', $id)->get()->first();
        $count = $videoinfo->view_count++;
        $update = Video::where('id', $id)->update([
            'view_count'=>($count+1)
        ]);
        $change =  Video::where('id', $id)->get()->first();
        $finalcount = $change->view_count;

        return response()->json(['data'=>$finalcount]);
    }

    public function getHistory($id)
    {
        $log = userlog::where('user_id', $id)->get()->first();
        if(!empty($log)) {
            $historyVideo = unserialize($log->log);
            $i = 0;
            foreach ($historyVideo as $vid) {
                if(Video::find($vid)!=null) {
                    $vid = Video::where('id', $vid)->get()->first();
                    $user = User::where('id', $vid->user_id)->get()->first();
                    $role = Role::where('user_id', $user->id)->get()->first();
                    $videos[$i] = [
                        'id'=>$vid->id,
                        'videoName'=>$vid->name,
                        'img'=>$vid->img,
                        'role'=>$role->role,
                        'uploader'=>$user->name,
                    ];
                    $i++;
                }
            }
            return view('frontend.pages.user.history', compact('videos'));
        }
        else {
            return view('frontend.pages.user.history');
        }

    }

    public function insertdata()
    {
        $category = Category::all();
        return view('frontend.pages.login.insert', compact('category'));
    }

    public function calculateIng(Request $request)
    {
        $ing = $request->input('ing');
        $unit = $request->input('unit');
        $qty = $request->input('qty');
        $videoId = $request->input('video');
        $people = $request->input('people');
        if($people==null)
            $people=1;
        for($i=0;$i<count($ing);$i++)
        {
            $totalqty = $qty[$i]*$people;
            if($unit[$i]=='gram')
            {
                if($totalqty>=1000) {
                    $totalqty /= 1000;
                    $unit[$i]="Kilogram";
                }
            }

            if($unit[$i]=='millilitre')
            {
                if($totalqty>=1000)
                {
                    $totalqty /=1000;
                    $unit[$i]="litre";
                }
            }
            if($unit[$i]=='millimeter')
            {
                if($totalqty>=10)
                {
                    $totalqty /=10;
                    $unit[$i]="centimeter";
                }
            }
            if($unit[$i]=='centimeter')
            {
                if($totalqty>=100)
                {
                    $totalqty /=100;
                    $unit[$i]="meter";
                }
            }
            $qty[$i]=$totalqty;
            $quantity[$i]=[
              'ing'=>$ing[$i],
              'qty'=>$qty[$i],
              'unit'=>$unit[$i]
            ];
        }


        if($videoId!=null) {
            $video = Video::where('id', $videoId)->get()->first();
        }
        else
            $video = null;
        return view('frontend.pages.user.calculateResult', compact('quantity', 'video', 'people'));

    }
}
