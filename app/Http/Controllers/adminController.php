<?php

namespace App\Http\Controllers;

use App\Role;
use App\Video;
use Illuminate\Http\Request;
use App\User;

class adminController extends Controller
{
    public function listUser()
    {
        $list=User::all();
        $i=0;
        foreach($list as $l)
        {
            $role = Role::where('user_id', $l->id)->get()->first();
            $lists[$i]= [
                'id'=>$l->id,
                'name'=>$l->name,
                'role'=>$role->role,
                'email'=>$l->email
            ];
            $i++;
        }
        return view('frontend.pages.admin.userlist', compact('lists'));
    }

    public function listVideo()
    {
        $list = Video::where('id','>','1')->with('users')->get();
        return view('frontend.pages.admin.videolist', compact('list'));
    }

    public function changerole($id)
    {
        $role = Role::where('user_id', $id)->get()->first();
        if($role->role==1)
            $r = 2;
        else
            $r=1;
        $update = Role::where('user_id', $id)->update([
            'role'=>$r
        ]);
        return redirect()->route('list.user');
    }
}
