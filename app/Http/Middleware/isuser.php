<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\User;
use App\Role;

class isuser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $email=Auth::User()->email;
        $id=User::where('email', $email)->get();
        $role=Role::where('user_id', $id[0]['id'])->get();
        if($role[0]['role']==0)
        {
            //return redirect('/list');
            return redirect()->route('videopage');
        }
        return $next($request);
    }
}
