<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class userlog extends Model
{
    protected $table = 'userlog';

    protected $fillable = ['user_id', 'log'];

    protected $hidden = [''];

}
