<?php namespace App;

/**
 * Eloquent class to describe the role table
 *
 * automatically generated by ModelGenerator.php
 */
class Role extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'role';

    public $timestamps = false;

    protected $fillable = array('role');

    public function users()
    {
        return $this->belongsTo('App\Users', 'user_id', 'id');
    }
}
